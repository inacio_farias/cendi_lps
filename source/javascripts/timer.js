/******************************************************************************************************************************
COMMING SOON PAGE
*******************************************************************************************************************************/
(function($) {
  var timer = $('#timer');
  if (timer.length) {
    /**
    * Set your date here  (YEAR, MONTH (0 for January/11 for December), DAY, HOUR, MINUTE, SECOND)
    * according to the GMT+0 Timezone
    **/
    var launch = new Date(2018, 02, 06, 20, 00, 00);
    /**
    * The script
    **/
    var days = $('#days');
    var hours = $('#hours');
    var minutes = $('#minutes');
    var seconds = $('#seconds');

    setDate();
    function setDate(){
      var now = new Date();
      if( launch < now ){
        days.html('<h1>0</H1><p>Dia</p>');
        hours.html('<h1>0</h1><p>Hora</p>');
        minutes.html('<h1>0</h1><p>Minuto</p>');
        seconds.html('<h1>0</h1><p>Segundo</p>');
      }
      else{
        var s = -now.getTimezoneOffset()*60 + (launch.getTime() - now.getTime())/1000;
        var d = Math.floor(s/86400);
        days.html('<h1>'+d+'</h1><p>Dia'+(d>1?'s':''),'</p>');
        s -= d*86400;

        var h = Math.floor(s/3600);
        hours.html('<h1>'+h+'</h1><p>Hora'+(h>1?'s':''),'</p>');
        s -= h*3600;

        var m = Math.floor(s/60);
        minutes.html('<h1>'+m+'</h1><p>Minuto'+(m>1?'s':''),'</p>');

        s = Math.floor(s-m*60);
        seconds.html('<h1>'+s+'</h1><p>Segundo'+(s>1?'s':''),'</p>');
        setTimeout(setDate, 1000);
      }
    }
  }
})(jQuery);
